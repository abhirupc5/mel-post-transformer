package com.commutair.nfp.amos.melbook.transformer;

public interface MelCdlPostRequestPublisher {
	
	/**
	 * This method is responsible for publishing exception to queue
	 * 
	 * @param payload
	 */
	public void publish(final String payload, final String correlationId);
}
