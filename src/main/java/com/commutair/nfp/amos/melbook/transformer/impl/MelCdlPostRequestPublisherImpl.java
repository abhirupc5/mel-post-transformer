package com.commutair.nfp.amos.melbook.transformer.impl;

import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.CDL_CONSTANT;
import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.GROUP_CONSTANT;
import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.MEL_CONSTANT;
import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.MESSAGE_GROUP_ID;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.stereotype.Service;

import com.commutair.nfp.amos.melbook.domain.QueuePayload;
import com.commutair.nfp.amos.melbook.transformer.MelCdlPostRequestPublisher;

@Service
public class MelCdlPostRequestPublisherImpl implements MelCdlPostRequestPublisher {

	@Autowired
	private QueueMessagingTemplate messagingTemplate;
	
	@Value("${melcdl-publish-queue-url}")
	private String publishQueue;
	
	@Override
	public void publish(final String payload, final String correlationId) {
		QueuePayload queuePayload = new QueuePayload();
		queuePayload.setPayload(payload);
		queuePayload.setCorrelationId(correlationId);
		Map<String, Object> headers = new HashMap<>();
	    headers.put(MESSAGE_GROUP_ID, String.format("%s-%s-%s", MEL_CONSTANT, CDL_CONSTANT, GROUP_CONSTANT));
		messagingTemplate.convertAndSend(publishQueue, queuePayload, headers);
	}
}
