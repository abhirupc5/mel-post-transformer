package com.commutair.nfp.amos.melbook.controller;

import static com.commutair.nfp.amos.melbook.util.MelCdlConstants.CORRELATION_ID_CONSTANT;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.commutair.nfp.amos.melbook.domain.Response;
import com.commutair.nfp.amos.melbook.transformer.MelCdlPostRequestPublisher;

@RestController
public class MelCdlPostRequestController {
	
	private Logger logger = LoggerFactory.getLogger(MelCdlPostRequestController.class);
	
	@Autowired
	private MelCdlPostRequestPublisher publisher;
	
	/**
	 * @param payload
	 * @return
	 */
	@RequestMapping(value = "/amos/post/transform", method = RequestMethod.POST)
	public ResponseEntity<Response> transform(@RequestHeader("correlationId") final String correlationId, final @RequestBody String payload) {
		MDC.put(CORRELATION_ID_CONSTANT, "[" + correlationId + "]");
		logger.info(String.format("Consumed message %s", payload));
		Response response = null;
		try {
			publisher.publish(payload, correlationId);
			response = new Response(0, payload, HttpStatus.ACCEPTED);
			response.setCorrelationId(correlationId);
		} catch (Exception e) {
			final List<String> collect = new ArrayList<String>();
			collect.add(e.getCause().toString());
			collect.addAll(Stream.of(e.getStackTrace()).map(st -> st.toString()).collect(Collectors.toList()));
			response = new Response(0, String.join("\n", collect), HttpStatus.BAD_REQUEST);
			response.setCorrelationId(correlationId);
			return ResponseEntity.badRequest().body(response);
		}
		return ResponseEntity.ok().body(response);
	}
}
